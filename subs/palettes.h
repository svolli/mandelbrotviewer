#ifndef OPC_PALETTES_H
#define OPC_PALETTES_H OPC_PALETTES_H

#ifdef __cplusplus
#include <cstdint>
#include <cstring>
extern "C"
{
#else
#include <stdint.h>
#include <string.h>
#endif

extern const uint32_t *palette_accent;
extern const uint32_t *palette_blues;
extern const uint32_t *palette_brbg;
extern const uint32_t *palette_bugn;
extern const uint32_t *palette_bupu;
extern const uint32_t *palette_dark2;
extern const uint32_t *palette_gnbu;
extern const uint32_t *palette_gnpu;
extern const uint32_t *palette_greens;
extern const uint32_t *palette_greys;
extern const uint32_t *palette_hsv11r;
extern const uint32_t *palette_hsv12r;
extern const uint32_t *palette_hsv13r;
extern const uint32_t *palette_hsv11l;
extern const uint32_t *palette_hsv12l;
extern const uint32_t *palette_hsv13l;
extern const uint32_t *palette_hsv21r;
extern const uint32_t *palette_hsv22r;
extern const uint32_t *palette_hsv23r;
extern const uint32_t *palette_hsv21l;
extern const uint32_t *palette_hsv22l;
extern const uint32_t *palette_hsv23l;
extern const uint32_t *palette_jet;
extern const uint32_t *palette_oranges;
extern const uint32_t *palette_orrd;
extern const uint32_t *palette_paired;
extern const uint32_t *palette_parula;
extern const uint32_t *palette_pastel1;
extern const uint32_t *palette_pastel2;
extern const uint32_t *palette_petrol;
extern const uint32_t *palette_piyg;
extern const uint32_t *palette_prgn;
extern const uint32_t *palette_pubugn;
extern const uint32_t *palette_pubu;
extern const uint32_t *palette_puor;
extern const uint32_t *palette_purd;
extern const uint32_t *palette_purples;
extern const uint32_t *palette_rdbu;
extern const uint32_t *palette_rdgy;
extern const uint32_t *palette_rdpu;
extern const uint32_t *palette_rdylbu;
extern const uint32_t *palette_rdylgn;
extern const uint32_t *palette_reds;
extern const uint32_t *palette_set1;
extern const uint32_t *palette_set2;
extern const uint32_t *palette_set3;
extern const uint32_t *palette_spectral;
extern const uint32_t *palette_whylrd;
extern const uint32_t *palette_ylgnbu;
extern const uint32_t *palette_ylgn;
extern const uint32_t *palette_ylorbr;
extern const uint32_t *palette_ylorrd;
extern const uint32_t *palette_ylrd;

extern const uint32_t *palette_vcsntsc;

const char *pal2name( const uint32_t *pointer );

void pal_list_init();
unsigned int pal_list_elements();
const char *pal_list_name( int i );
const uint32_t *pal_list_data( int i );
unsigned int pal_list_id( const char *name, unsigned int notfound );

#ifdef __cplusplus
}
#endif

#endif
