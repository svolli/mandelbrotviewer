#ifndef LIBOPC_HSV_H
#define LIBOPC_HSV_H LIBOPC_HSV_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
   float h;
   float s;
   float v;
} hsv_t;

extern unsigned int hsv_to_rgb( const hsv_t *hsv );
extern void rgb_to_hsv( hsv_t *hsv, unsigned int rgb );

#ifdef __cplusplus
}
#endif

#endif

