#include "mandelbrot.h"
#include "hsv.h"

static void interpolate_gradient( unsigned int *d, unsigned int size,
                                  unsigned int start, unsigned int end )
{
   hsv_t hsv_start;
   hsv_t hsv_end;
   float h_step;
   float s_step;
   float v_step;
   int i;

   rgb_to_hsv( &hsv_start, start );
   rgb_to_hsv( &hsv_end,   end   );

   h_step = (hsv_end.h - hsv_start.h);
   if( h_step < -0.5 )
   {  
      h_step += 1.0;
   }  
   if( h_step > 0.5 )
   {
      h_step -= 1.0;
   }
   h_step /= size;
   s_step = (hsv_end.s - hsv_start.s) / size;
   v_step = (hsv_end.v - hsv_start.v) / size;

   for( i = 0; i < size; ++i )
   {
      hsv_start.h += h_step;
      hsv_start.s += s_step;
      hsv_start.v += v_step;

      if( hsv_start.h > 1.0 )
      {
         hsv_start.h -= 1.0;
      }
      if( hsv_start.h < 0.0 )
      {
         hsv_start.h += 1.0;
      }

      *(d++) = hsv_to_rgb( &hsv_start );
   }
}

void make_gradient( unsigned int *dest, unsigned int num_elems, unsigned int const *palette )
{
   unsigned int src_elems = palette[0];
   unsigned int steps = num_elems / (src_elems - 1);
   unsigned int left  = num_elems - (src_elems - 2) * (steps);

   unsigned int i;

   for( i = 0; i < src_elems-2; ++i )
   {
      interpolate_gradient( &dest[i*steps], steps, palette[1+i], palette[2+i] );
   }
   interpolate_gradient( &dest[num_elems-left], left, palette[src_elems-1], palette[src_elems] );
}

