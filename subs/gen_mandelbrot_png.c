
#include "gen_mandelbrot_png.h"
#include <stdio.h>
#include <stdlib.h>

void gen_mandelbrot_png(
   const char *filename,
   int width,
   int height,
   int colors,
   const uint32_t *palette,
   mandelbrot_float xpos,
   mandelbrot_float ypos,
   mandelbrot_float zoom
   )
{
   gdImagePtr im;
   FILE *f;
   mandelbrot_t m;

   im = gdImageCreateTrueColor( width, height );
   m = mandelbrot_init( im, 1, colors );
   mandelbrot_setparams( m, zoom, xpos, ypos );
   mandelbrot_setpalette( m, palette );
   mandelbrot_calc( m );
   mandelbrot_done( m );

   f = fopen( filename, "wb" );
   if( f )
   {
      gdImagePng( im, f );
      fclose( f );
      f = NULL;
   }
   gdImageDestroy( im );
}
