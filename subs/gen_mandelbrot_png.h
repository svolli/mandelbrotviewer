
#ifndef GEN_MANDELBROT_PNG_H
#define GEN_MANDELBROT_PNG_H GEN_MANDELBROT_PNG_H

#include "mandelbrot.h"
#include "palettes.h"

#ifdef __cplusplus
extern "C" {
#endif

void gen_mandelbrot_png(
   const char *filename,
   int width,
   int height,
   int colors,
   const uint32_t *palette,
   mandelbrot_float xpos,
   mandelbrot_float ypos,
   mandelbrot_float zoom
   )
;
#ifdef __cplusplus
}
#endif

#endif
