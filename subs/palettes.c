#include "palettes.h"

static const uint32_t palette_accent_data[] = {
   8,
   0x7FC97F,
   0xBEAED4,
   0xFDC086,
   0xFFFF99,

   0x386CB0,
   0xF0027F,
   0xBF5B17,
   0x666666,
};
const uint32_t *palette_accent = palette_accent_data;

static const uint32_t palette_blues_data[] = {
   8,
   0xF7FBFF,
   0xDEEBF7,
   0xC6DBEF,
   0x9ECAE1,

   0x6BAED6,
   0x4292C6,
   0x2171B5,
   0x084594,
};
const uint32_t *palette_blues = palette_blues_data;

static const uint32_t palette_brbg_data[] = {
   8,
   0x8C510A,
   0xBF812D,
   0xDFC27D,
   0xF6E8C3,

   0xC7EAE5,
   0x80CDC1,
   0x35978F,
   0x01665E,
};
const uint32_t *palette_brbg = palette_brbg_data;

static const uint32_t palette_bugn_data[] = {
   8,
   0xF7FCFD,
   0xE5F5F9,
   0xCCECE6,
   0x99D8C9,

   0x66C2A4,
   0x41AE76,
   0x238B45,
   0x005824,
};
const uint32_t *palette_bugn = palette_bugn_data;

static const uint32_t palette_bupu_data[] = {
   8,
   0xF7FCFD,
   0xE0ECF4,
   0xBFD3E6,
   0x9EBCDA,

   0x8C96C6,
   0x8C6BB1,
   0x88419D,
   0x6E016B,
};
const uint32_t *palette_bupu = palette_bupu_data;

static const uint32_t palette_dark2_data[] = {
   8,
   0x1B9E77,
   0xD95F02,
   0x7570B3,
   0xE7298A,

   0x66A61E,
   0xE6AB02,
   0xA6761D,
   0x666666,
};
const uint32_t *palette_dark2 = palette_dark2_data;

static const uint32_t palette_gnbu_data[] = {
   8,
   0xF7FCF0,
   0xE0F3DB,
   0xCCEBC5,
   0xA8DDB5,

   0x7BCCC4,
   0x4EB3D3,
   0x2B8CBE,
   0x08589E,
};
const uint32_t *palette_gnbu = palette_gnbu_data;

static const uint32_t palette_gnpu_data[] = {
   10,
   0x396353,
   0x0db14b,
   0x6dc067,
   0xabd69b,

   0xdaeac1,
   0xdfcce4,
   0xc7b2d6,
   0x9474b4,

   0x754098,
   0x504971,
};
const uint32_t *palette_gnpu = palette_gnpu_data;

static const uint32_t palette_greens_data[] = {
   8,
   0xF7FCF5,
   0xE5F5E0,
   0xC7E9C0,
   0xA1D99B,

   0x74C476,
   0x41AB5D,
   0x238B45,
   0x005A32,
};
const uint32_t *palette_greens = palette_greens_data;

static const uint32_t palette_greys_data[] = {
   8,
   0xFFFFFF,
   0xF0F0F0,
   0xD9D9D9,
   0xBDBDBD,

   0x969696,
   0x737373,
   0x525252,
   0x252525,
};
const uint32_t *palette_greys = palette_greys_data;

static const uint32_t palette_hsv11l_data[] = {
   4,
   0x0000ff,
   0x00ff00,
   0xff0000,
   0x0000ff,
};

const uint32_t *palette_hsv11l = palette_hsv11l_data;

static const uint32_t palette_hsv11r_data[] = {
   4,
   0xff0000,
   0x00ff00,
   0x0000ff,
   0xff0000,
};

const uint32_t *palette_hsv11r = palette_hsv11r_data;

static const uint32_t palette_hsv12l_data[] = {
   4,
   0x00ff00,
   0xff0000,
   0x0000ff,
   0x00ff00,
};

const uint32_t *palette_hsv12l = palette_hsv12l_data;

static const uint32_t palette_hsv12r_data[] = {
   4,
   0x00ff00,
   0x0000ff,
   0xff0000,
   0x00ff00,
};

const uint32_t *palette_hsv12r = palette_hsv12r_data;

static const uint32_t palette_hsv13l_data[] = {
   4,
   0xff0000,
   0x0000ff,
   0x00ff00,
   0xff0000,
};

const uint32_t *palette_hsv13l = palette_hsv13l_data;

static const uint32_t palette_hsv13r_data[] = {
   4,
   0x0000ff,
   0xff0000,
   0x00ff00,
   0x0000ff,
};

const uint32_t *palette_hsv13r = palette_hsv13r_data;

static const uint32_t palette_hsv21l_data[] = {
   4,
   0xffff00,
   0xff00ff,
   0x00ffff,
   0xffff00,
};

const uint32_t *palette_hsv21l = palette_hsv21l_data;

static const uint32_t palette_hsv21r_data[] = {
   4,
   0x00ffff,
   0xff00ff,
   0xffff00,
   0x00ffff,
};

const uint32_t *palette_hsv21r = palette_hsv21r_data;

static const uint32_t palette_hsv22l_data[] = {
   4,
   0xff00ff,
   0x00ffff,
   0xffff00,
   0xff00ff,
};

const uint32_t *palette_hsv22l = palette_hsv22l_data;

static const uint32_t palette_hsv22r_data[] = {
   4,
   0xff00ff,
   0xffff00,
   0x00ffff,
   0xff00ff,
};

const uint32_t *palette_hsv22r = palette_hsv22r_data;

static const uint32_t palette_hsv23l_data[] = {
   4,
   0x00ffff,
   0xffff00,
   0xff00ff,
   0x00ffff,
};

const uint32_t *palette_hsv23l = palette_hsv23l_data;

static const uint32_t palette_hsv23r_data[] = {
   4,
   0xffff00,
   0x00ffff,
   0xff00ff,
   0xffff00,
};

const uint32_t *palette_hsv23r = palette_hsv23r_data;

static const uint32_t palette_jet_data[] = {
   9,
   0x000080,
   0x0000ff,
   0x0080ff,
   0x00ffff,
   
   0x80ff80,
   0xffff00,
   0xff8000,
   0xff0000,
   
   0x800000,
};
const uint32_t *palette_jet = palette_jet_data;

static const uint32_t palette_oranges_data[] = {
   8,
   0xFFF5EB,
   0xFEE6CE,
   0xFDD0A2,
   0xFDAE6B,

   0xFD8D3C,
   0xF16913,
   0xD94801,
   0x8C2D04,
};
const uint32_t *palette_oranges = palette_oranges_data;

static const uint32_t palette_orrd_data[] = {
   8,
   0xFFF7EC,
   0xFEE8C8,
   0xFDD49E,
   0xFDBB84,

   0xFC8D59,
   0xEF6548,
   0xD7301F,
   0x990000,
};
const uint32_t *palette_orrd = palette_orrd_data;

static const uint32_t palette_paired_data[] = {
   8,
   0xA6CEE3,
   0x1F78B4,
   0xB2DF8A,
   0x33A02C,

   0xFB9A99,
   0xE31A1C,
   0xFDBF6F,
   0xFF7F00,
};
const uint32_t *palette_paired = palette_paired_data;

static const uint32_t palette_parula_data[] = {
   9,
   0x352a87,
   0x0363e1,
   0x1485d4,
   0x06a7c6,

   0x38b99e,
   0x92bf73,
   0xd9ba56,
   0xfcce2e,
   0xf9fb0e,
};
const uint32_t *palette_parula = palette_parula_data;

static const uint32_t palette_pastel1_data[] = {
   8,
   0xFBB4AE,
   0xB3CDE3,
   0xCCEBC5,
   0xDECBE4,

   0xFED9A6,
   0xFFFFCC,
   0xE5D8BD,
   0xFDDAEC,
};
const uint32_t *palette_pastel1 = palette_pastel1_data;

static const uint32_t palette_pastel2_data[] = {
   8,
   0xB3E2CD,
   0xFDCDAC,
   0xCDB5E8,
   0xF4CAE4,

   0xD6F5C9,
   0xFFF2AE,
   0xF1E2CC,
   0xCCCCCC,
};
const uint32_t *palette_pastel2 = palette_pastel2_data;

static const uint32_t palette_petrol_data[] = {
   4,
   0x00E4F5,
   0x00A9B5,
   0x006D75,
   0x004A4F,
};
const uint32_t *palette_petrol = palette_petrol_data;

static const uint32_t palette_piyg_data[] = {
   8,
   0xC51B7D,
   0xDE77AE,
   0xF1B6DA,
   0xFDE0EF,

   0xE6F5D0,
   0xB8E186,
   0x7FBC41,
   0x4D9221,
};
const uint32_t *palette_piyg = palette_piyg_data;

static const uint32_t palette_prgn_data[] = {
   8,
   0x762A83,
   0x9970AB,
   0xC2A5CF,
   0xE7D4E8,

   0xD9F0D3,
   0xA6DBA0,
   0x5AAE61,
   0x1B7837,
};
const uint32_t *palette_prgn = palette_prgn_data;

static const uint32_t palette_pubu_data[] = {
   8,
   0xFFF7FB,
   0xECE7F2,
   0xD0D1E6,
   0xA6BDDB,

   0x74A9CF,
   0x3690C0,
   0x0570B0,
   0x034E7B,
};
const uint32_t *palette_pubu = palette_pubu_data;

static const uint32_t palette_pubugn_data[] = {
   8,
   0xFFF7FB,
   0xECE7F0,
   0xD0D1E6,
   0xA6BDDB,

   0x67A9CF,
   0x3690C0,
   0x02818A,
   0x016540,
};
const uint32_t *palette_pubugn = palette_pubugn_data;

static const uint32_t palette_puor_data[] = {
   8,
   0xB35806,
   0xE08214,
   0xFDB863,
   0xFEE0B6,

   0xD8DAEB,
   0xB2ABD2,
   0x8073AC,
   0x542788,
};
const uint32_t *palette_puor = palette_puor_data;

static const uint32_t palette_purd_data[] = {
   8,
   0xF7F4F9,
   0xE7E1EF,
   0xD4B9DA,
   0xC994C7,

   0xDF65B0,
   0xE7298A,
   0xCE1256,
   0x91003F,
};
const uint32_t *palette_purd = palette_purd_data;

static const uint32_t palette_purples_data[] = {
   8,
   0xFCFBFD,
   0xEFEDF5,
   0xDADAEB,
   0xBCBDDC,

   0x9E9AC8,
   0x807DBA,
   0x6A51A3,
   0x4A1486,
};
const uint32_t *palette_purples = palette_purples_data;

static const uint32_t palette_rdbu_data[] = {
   8,
   0xB2182B,
   0xD6604D,
   0xF4A582,
   0xFDDBC7,

   0xD1E5F0,
   0x92C5DE,
   0x4393C3,
   0x2166AC,
};
const uint32_t *palette_rdbu = palette_rdbu_data;

static const uint32_t palette_rdgy_data[] = {
   8,
   0xB2182B,
   0xD6604D,
   0xF4A582,
   0xFDDBC7,

   0xE0E0E0,
   0xBABABA,
   0x878787,
   0x4D4D4D,
};
const uint32_t *palette_rdgy = palette_rdgy_data;

static const uint32_t palette_rdpu_data[] = {
   8,
   0xFFF7F3,
   0xFDE0DD,
   0xFCC5C0,
   0xFA9FB5,

   0xF768A1,
   0xDD3497,
   0xAE017E,
   0x7A0177,
};
const uint32_t *palette_rdpu = palette_rdpu_data;

static const uint32_t palette_rdylbu_data[] = {
   8,
   0xD73027,
   0xF46D43,
   0xFDAE61,
   0xFEE090,

   0xE0F3F8,
   0xABD9E9,
   0x74ADD1,
   0x4575B4,
};
const uint32_t *palette_rdylbu = palette_rdylbu_data;

static const uint32_t palette_rdylgn_data[] = {
   8,
   0xD73027,
   0xF46D43,
   0xFDAE61,
   0xFEE08B,

   0xD9EF8B,
   0xA6D96A,
   0x66BD63,
   0x1A9850,
};
const uint32_t *palette_rdylgn = palette_rdylgn_data;

static const uint32_t palette_reds_data[] = {
   8,
   0xFFF5F0,
   0xFEE0D2,
   0xFCBBA1,
   0xFC9272,

   0xFB6A4A,
   0xEF3B2C,
   0xCB181D,
   0x99000D,
};
const uint32_t *palette_reds = palette_reds_data;

static const uint32_t palette_set1_data[] = {
   8,
   0xE41A1C,
   0x377EB8,
   0x4DAF4A,
   0x984EA3,

   0xFF7F00,
   0xFFFF33,
   0xA65628,
   0xF781BF,
};
const uint32_t *palette_set1 = palette_set1_data;

static const uint32_t palette_set2_data[] = {
   8,
   0x66C2A5,
   0xFC8D62,
   0x8DA0CB,
   0xE78AC3,

   0xA6D854,
   0xFFD92F,
   0xE5C494,
   0xB3B3B3,
};
const uint32_t *palette_set2 = palette_set2_data;

static const uint32_t palette_set3_data[] = {
   8,
   0x8DD3C7,
   0xFFFFB3,
   0xBEBADA,
   0xFB8072,

   0x80B1D3,
   0xFDB462,
   0xB3DE69,
   0xFCCDE5,
};
const uint32_t *palette_set3 = palette_set3_data;

static const uint32_t palette_spectral_data[] = {
   8,
   0xD53E4F,
   0xF46D43,
   0xFDAE61,
   0xFEE08B,

   0xE6F598,
   0xABDDA4,
   0x66C2A5,
   0x3288BD,
};
const uint32_t *palette_spectral = palette_spectral_data;

static const uint32_t palette_vcsntsc_data[] = {
   0x100,
   0x000000,0x4a4a4a,0x6f6f6f,0x8e8e8e,0xaaaaaa,0xc0c0c0,0xd6d6d6,0xececec,
   0xececec,0xd6d6d6,0xc0c0c0,0xaaaaaa,0x8e8e8e,0x6f6f6f,0x4a4a4a,0x000000,
   0x484800,0x69690f,0x86861d,0xa2a22a,0xbbbb35,0xd2d240,0xe8e84a,0xfcfc54,
   0xfcfc54,0xe8e84a,0xd2d240,0xbbbb35,0xa2a22a,0x86861d,0x69690f,0x484800,
   0x7c2c00,0x904811,0xa26221,0xb47a30,0xc3903d,0xd2a44a,0xdfb755,0xecc860,
   0xecc860,0xdfb755,0xd2a44a,0xc3903d,0xb47a30,0xa26221,0x904811,0x7c2c00,
   0x901c00,0xa33915,0xb55328,0xc66c3a,0xd5824a,0xe39759,0xf0aa67,0xfcbc74,
   0xfcbc74,0xf0aa67,0xe39759,0xd5824a,0xc66c3a,0xb55328,0xa33915,0x901c00,
   0x940000,0xa71a1a,0xb83232,0xc84848,0xd65c5c,0xe46f6f,0xf08080,0xfc9090,
   0xfc9090,0xf08080,0xe46f6f,0xd65c5c,0xc84848,0xb83232,0xa71a1a,0x940000,
   0x840064,0x97197a,0xa8308f,0xb846a2,0xc659b3,0xd46cc3,0xe07cd2,0xec8ce0,
   0xec8ce0,0xe07cd2,0xd46cc3,0xc659b3,0xb846a2,0xa8308f,0x97197a,0x840064,
   0x500084,0x68199a,0x7d30ad,0x9246c0,0xa459d0,0xb56ce0,0xc57cee,0xd48cfc,
   0xd48cfc,0xc57cee,0xb56ce0,0xa459d0,0x9246c0,0x7d30ad,0x68199a,0x500084,
   0x140090,0x331aa3,0x4e32b5,0x6848c6,0x7f5cd5,0x956fe3,0xa980f0,0xbc90fc,
   0xbc90fc,0xa980f0,0x956fe3,0x7f5cd5,0x6848c6,0x4e32b5,0x331aa3,0x140090,
   0x000094,0x181aa7,0x2d32b8,0x4248c8,0x545cd6,0x656fe4,0x7580f0,0x8490fc,
   0x8490fc,0x7580f0,0x656fe4,0x545cd6,0x4248c8,0x2d32b8,0x181aa7,0x000094,
   0x001c88,0x183b9d,0x2d57b0,0x4272c2,0x548ad2,0x65a0e1,0x75b5ef,0x84c8fc,
   0x84c8fc,0x75b5ef,0x65a0e1,0x548ad2,0x4272c2,0x2d57b0,0x183b9d,0x001c88,
   0x003064,0x185080,0x2d6d98,0x4288b0,0x54a0c5,0x65b7d9,0x75cceb,0x84e0fc,
   0x84e0fc,0x75cceb,0x65b7d9,0x54a0c5,0x4288b0,0x2d6d98,0x185080,0x003064,
   0x004030,0x18624e,0x2d8169,0x429e82,0x54b899,0x65d1ae,0x75e7c2,0x84fcd4,
   0x84fcd4,0x75e7c2,0x65d1ae,0x54b899,0x429e82,0x2d8169,0x18624e,0x004030,
   0x004400,0x1a661a,0x328432,0x48a048,0x5cba5c,0x6fd26f,0x80e880,0x90fc90,
   0x90fc90,0x80e880,0x6fd26f,0x5cba5c,0x48a048,0x328432,0x1a661a,0x004400,
   0x143c00,0x355f18,0x527e2d,0x6e9c42,0x87b754,0x9ed065,0xb4e775,0xc8fc84,
   0xc8fc84,0xb4e775,0x9ed065,0x87b754,0x6e9c42,0x527e2d,0x355f18,0x143c00,
   0x303800,0x505916,0x6d762b,0x88923e,0xa0ab4f,0xb7c25f,0xccd86e,0xe0ec7c,
   0xe0ec7c,0xccd86e,0xb7c25f,0xa0ab4f,0x88923e,0x6d762b,0x505916,0x303800,
   0x482c00,0x694d14,0x866a26,0xa28638,0xbb9f47,0xd2b656,0xe8cc63,0xfce070,
   0xfce070,0xe8cc63,0xd2b656,0xbb9f47,0xa28638,0x866a26,0x694d14,0x482c00
};
const uint32_t *palette_vcsntsc = palette_vcsntsc_data;

static const uint32_t palette_whylrd_data[] = {
   5,
   0xffffff,
   0xffee00,
   0xff7000,
   0xee0000,

   0x7f0000,
};
const uint32_t *palette_whylrd = palette_whylrd_data;

static const uint32_t palette_ylgnbu_data[] = {
   8,
   0xFFFFD9,
   0xEDF8B1,
   0xC7E9B4,
   0x7FCDBB,

   0x41B6C4,
   0x1D91C0,
   0x225EA8,
   0x0C2C84,
};
const uint32_t *palette_ylgnbu = palette_ylgnbu_data;

static const uint32_t palette_ylgn_data[] = {
   8,
   0xFFFFE5,
   0xF7FCB9,
   0xD9F0A3,
   0xADDD8E,

   0x78C679,
   0x41AB5D,
   0x238443,
   0x005A32,
};
const uint32_t *palette_ylgn = palette_ylgn_data;

static const uint32_t palette_ylorbr_data[] = {
   8,
   0xFFFFE5,
   0xFFF7BC,
   0xFEE391,
   0xFEC44F,

   0xFE9929,
   0xEC7014,
   0xCC4C02,
   0x8C2D04,
};
const uint32_t *palette_ylorbr = palette_ylorbr_data;

static const uint32_t palette_ylorrd_data[] = {
   8,
   0xFFFFCC,
   0xFFEDA0,
   0xFED976,
   0xFEB24C,

   0xFD8D3C,
   0xFC4E2A,
   0xE31A1C,
   0xB10026,
};
const uint32_t *palette_ylorrd = palette_ylorrd_data;

static const uint32_t palette_ylrd_data[] = {
   4,
   0xffee00,
   0xff7000,
   0xee0000,
   0x7f0000,
};
const uint32_t *palette_ylrd = palette_ylrd_data;
