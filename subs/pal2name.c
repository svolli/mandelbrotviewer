
#include "palettes.h"

const char *pal2name( const uint32_t *pointer )
{
   if( pointer == palette_accent)    return "accent";
   if( pointer == palette_blues)     return "blues";
   if( pointer == palette_brbg)      return "brgb";
   if( pointer == palette_bugn)      return "bugn";
   if( pointer == palette_bupu)      return "bupu";
   if( pointer == palette_dark2)     return "dark2";
   if( pointer == palette_gnbu)      return "gnbu";
   if( pointer == palette_gnpu)      return "gnpu";
   if( pointer == palette_greens)    return "greens";
   if( pointer == palette_greys)     return "greys";
   if( pointer == palette_hsv11r)    return "hsv11r";
   if( pointer == palette_hsv12r)    return "hsv12r";
   if( pointer == palette_hsv13r)    return "hsv13r";
   if( pointer == palette_hsv11l)    return "hsv11l";
   if( pointer == palette_hsv12l)    return "hsv12l";
   if( pointer == palette_hsv13l)    return "hsv13l";
   if( pointer == palette_hsv21r)    return "hsv21r";
   if( pointer == palette_hsv22r)    return "hsv22r";
   if( pointer == palette_hsv23r)    return "hsv23r";
   if( pointer == palette_hsv21l)    return "hsv21l";
   if( pointer == palette_hsv22l)    return "hsv22l";
   if( pointer == palette_hsv23l)    return "hsv23l";
   if( pointer == palette_jet)       return "jet";
   if( pointer == palette_oranges)   return "oranges";
   if( pointer == palette_orrd)      return "orrd";
   if( pointer == palette_paired)    return "paired";
   if( pointer == palette_parula)    return "parula";
   if( pointer == palette_pastel1)   return "pastel1";
   if( pointer == palette_pastel2)   return "pastel2";
   if( pointer == palette_petrol)    return "petrol";
   if( pointer == palette_piyg)      return "piyg";
   if( pointer == palette_prgn)      return "prgn";
   if( pointer == palette_pubugn)    return "pubugn";
   if( pointer == palette_pubu)      return "pubu";
   if( pointer == palette_puor)      return "puor";
   if( pointer == palette_purd)      return "purd";
   if( pointer == palette_purples)   return "purples";
   if( pointer == palette_rdbu)      return "rdbu";
   if( pointer == palette_rdgy)      return "rdgy";
   if( pointer == palette_rdpu)      return "rdpu";
   if( pointer == palette_rdylbu)    return "rdybu";
   if( pointer == palette_rdylgn)    return "rdygn";
   if( pointer == palette_reds)      return "reds";
   if( pointer == palette_set1)      return "set1";
   if( pointer == palette_set2)      return "set2";
   if( pointer == palette_set3)      return "set3";
   if( pointer == palette_spectral)  return "spectral";
   if( pointer == palette_whylrd)    return "whylrd";
   if( pointer == palette_ylgnbu)    return "ylgnbu";
   if( pointer == palette_ylgn)      return "ylgn";
   if( pointer == palette_ylorbr)    return "ylorbr";
   if( pointer == palette_ylorrd)    return "ylorrd";
   if( pointer == palette_ylrd)      return "ylrd";
   if( pointer == palette_vcsntsc)   return "vcsntsc";

   return "unknown";
}
