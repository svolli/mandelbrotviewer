
#include <stdlib.h>
#include <string.h>

#include "mandelbrot.h"

#define PALETTE_DEFAULT_SIZE (32)

struct mandelbrot_s
{
   gdImagePtr         im;
   gdImagePtr         work;
   int                oversampling;
   unsigned int       palette_min;
   unsigned int       palette_max;
   mandelbrot_float   x;
   mandelbrot_float   y;
   mandelbrot_float   zoom;
   unsigned int       *palette;
   unsigned int       palette_size;
};

mandelbrot_t mandelbrot_init( gdImagePtr im, int oversampling, int palette_size )
{
   mandelbrot_t d = (mandelbrot_t)malloc( sizeof( struct mandelbrot_s ) );
   d->im = im;
   d->oversampling = oversampling;
   d->zoom    = 2.0; /* set with 0.5 */
   d->x       = 0.0;
   d->y       = 0.0;
   if( palette_size < 2 )
   {
      palette_size = PALETTE_DEFAULT_SIZE;
   }
   d->palette_size = palette_size;
   d->palette = (unsigned int*)malloc( sizeof(unsigned int) * d->palette_size );
   if( oversampling > 1 )
   {
      d->work = gdImageCreateTrueColor( im->sx * oversampling, im->sy * oversampling );
   }
   else
   {
      d->work = d->im;
   }
   return d;
}

void mandelbrot_done( mandelbrot_t d )
{
   if( d->im != d->work )
   {
      gdImageDestroy( d->work );
   }
   free( d->palette );
   memset( d, 0, sizeof(*d) );
   free( d );
}

void mandelbrot_setparams( mandelbrot_t d, mandelbrot_float zoom,
                           mandelbrot_float x, mandelbrot_float y )
{
   d->zoom = 1.0 / zoom;
   d->x = x;
   d->y = y;
}

void mandelbrot_setpalette( mandelbrot_t d, const unsigned int *palette )
{
   make_gradient( d->palette, d->palette_size, palette );
}

unsigned int mandelbrot_minpalette( mandelbrot_t d )
{
   return d->palette_min;
}

unsigned int mandelbrot_maxpalette( mandelbrot_t d )
{
   return d->palette_max;
}

void mandelbrot_calc( mandelbrot_t d )
{
   mandelbrot_float xf, yf, r, i;
   mandelbrot_float left;
   mandelbrot_float top;
   mandelbrot_float dd;

   int xp, yp, count;

   d->palette_min = d->palette_size;
   d->palette_max = 0;

   if( d->work->sy > d->work->sx )
   {
      left = d->x - ((d->zoom * d->work->sx) / d->work->sy);
      top  = d->y - d->zoom;
      dd   = 2.0 * d->zoom / d->work->sy;
   }
   else
   {
      left = d->x - d->zoom;
      top  = d->y - ((d->zoom * d->work->sy) / d->work->sx);
      dd   = 2.0 * d->zoom / d->work->sx;
   }

   for( yp = 0; yp < d->work->sy; ++yp )
   {
      yf = top + (yp * dd);
      for( xp = 0; xp < d->work->sx; ++xp )
      {
         xf = left + (xp * dd);
         r = 0;
         i = 0;
         for( count = 0; count < d->palette_size; ++count )
         {
            mandelbrot_float nr = (r * r) - (i * i) + xf;
            mandelbrot_float ni = 2.0 * r * i + yf;
            r = nr;
            i = ni;
            if( (r < -2.5) || (r > 2.5) || (i < -2.0) || (i > 2.0) )
            {
               break;
            }
         }
         if( count < d->palette_size )
         {
            if( count > d->palette_max )
            {
               d->palette_max = count;
            }
            if( count < d->palette_min )
            {
               d->palette_min = count;
            }
            gdImageSetPixel( d->work, xp, yp, d->palette[count] );
         }
         else
         {
            gdImageSetPixel( d->work, xp, yp, 0 );
         }
      }
   }

   if( d->im != d->work )
   {
      gdImageCopyResampled( d->im, d->work, 0, 0, 0, 0, d->im->sx, d->im->sy, d->work->sx, d->work->sy );
   }
}
