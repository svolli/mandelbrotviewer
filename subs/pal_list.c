
#include "palettes.h"

#include <assert.h>
#include <stdlib.h>

static uint32_t **palettes = 0;
static unsigned int palettes_elements = 0;

void pal_list_init()
{
   const uint32_t *_palettes[] =
   {
      palette_accent,
      palette_blues,
      palette_brbg,
      palette_bugn,
      palette_bupu,
      palette_dark2,
      palette_gnbu,
      palette_gnpu,
      palette_greens,
      palette_greys,
      palette_hsv11r,
      palette_hsv12r,
      palette_hsv13r,
      palette_hsv11l,
      palette_hsv12l,
      palette_hsv13l,
      palette_hsv21r,
      palette_hsv22r,
      palette_hsv23r,
      palette_hsv21l,
      palette_hsv22l,
      palette_hsv23l,
      palette_jet,
      palette_oranges,
      palette_orrd,
      palette_paired,
      palette_parula,
      palette_pastel1,
      palette_pastel2,
      palette_petrol,
      palette_piyg,
      palette_prgn,
      palette_pubugn,
      palette_pubu,
      palette_puor,
      palette_purd,
      palette_purples,
      palette_rdbu,
      palette_rdgy,
      palette_rdpu,
      palette_rdylbu,
      palette_rdylgn,
      palette_reds,
      palette_set1,
      palette_set2,
      palette_set3,
      palette_spectral,
      palette_whylrd,
      palette_ylgnbu,
      palette_ylgn,
      palette_ylorbr,
      palette_ylorrd,
      palette_ylrd,
      palette_vcsntsc,
   };
   palettes = (uint32_t**)malloc( sizeof(_palettes) );
   palettes_elements = sizeof(_palettes)/sizeof(_palettes[0]);
   memcpy( palettes, _palettes, sizeof(_palettes) );
}

unsigned int pal_list_elements()
{
   assert(palettes_elements);
   return palettes_elements;
}

const char *pal_list_name( int i )
{
   assert(palettes);
   return pal2name( palettes[i] );
}

const uint32_t *pal_list_data( int i )
{
   assert(palettes);
   return palettes[i];
}

unsigned int pal_list_id( const char *name, unsigned int notfound )
{
   int i;
   for( i = 0; i < pal_list_elements(); ++i )
   {
      if( !strcmp( pal_list_name(i), name ) )
      {
         return i;
      }
   }
   return notfound;
}
