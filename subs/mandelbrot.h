
#ifndef MANDELBROT_H
#define MANDELBROT_H MANDELBROT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <gd.h>

/* mandelbrot.c */
typedef double mandelbrot_float;
typedef struct mandelbrot_s *mandelbrot_t;
mandelbrot_t mandelbrot_init( gdImagePtr im, int oversampling, int palette_size );
void mandelbrot_done( mandelbrot_t d );
void mandelbrot_setparams( mandelbrot_t d, mandelbrot_float zoom,
                           mandelbrot_float x, mandelbrot_float y );
void mandelbrot_setpalette( mandelbrot_t d, const unsigned int *palette );
unsigned int mandelbrot_minpalette( mandelbrot_t d );
unsigned int mandelbrot_maxpalette( mandelbrot_t d );
void mandelbrot_calc( mandelbrot_t d );                                         

/* make_gradient.c */
extern void make_gradient( unsigned int *dest, unsigned int num_elems,
                           unsigned int const *palette );

#ifdef __cplusplus
}
#endif

#endif
