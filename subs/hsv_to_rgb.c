
#include "hsv.h"

unsigned int hsv_to_rgb( const hsv_t *hsv )
{
   float f;
   int r = 0, g = 0, b = 0;
   int i;
   unsigned char x1, x2, x3, x4;

   x4 = (unsigned char)( hsv->v * 255.0 );
   if( hsv->s == 0 )
   {
      r = x4;
      g = x4;
      b = x4;
   }
   i = (int)(hsv->h * 6.0);
   f = (hsv->h * 6.0) - i;
   i %= 6;
   x1 = (unsigned char)( 255.0 * (hsv->v * ( 1.0 - hsv->s )) );
   x2 = (unsigned char)( 255.0 * (hsv->v * ( 1.0 - hsv->s * f)) );
   x3 = (unsigned char)( 255.0 * (hsv->v * ( 1.0 - hsv->s * (1.0-f) )) );
   switch( i )
   {
      case 0:
         r = x4;
         g = x3;
         b = x1;
         break;
      case 1:
         r = x2;
         g = x4;
         b = x1;
         break;
      case 2:
         r = x1;
         g = x4;
         b = x3;
         break;
      case 3:
         r = x1;
         g = x2;
         b = x4;
         break;
      case 4:
         r = x3;
         g = x1;
         b = x4;
         break;
      case 5:
         r = x4;
         g = x1;
         b = x2;
         break;
   }
   
   return (r << 16) | (g << 8) | b;
}
