
#include "hsv.h"
#include <math.h>

void rgb_to_hsv( hsv_t *hsv, unsigned int rgb )
{
   float r = (rgb >> 16) & 0xff;
   float g = (rgb >>  8) & 0xff;
   float b = (rgb      ) & 0xff;
   float min, max, delta;
   r /= 255.0;
   g /= 255.0;
   b /= 255.0;

   min = fminf( fminf( r, g ), b );
   max = fmaxf( fmaxf( r, g ), b );
   hsv->v = max;            // v

   delta = max - min;

   if( max != 0.0 )
   {
      hsv->s = delta / max;      // s
   }
   else
   {
      // r = g = b = 0      // s = 0, v is undefined
      hsv->h = 0.0;
      hsv->s = 0.0;
      return;
   }

   if( r == max )
   {
      hsv->h = ( g - b ) / delta;      // between yellow & magenta
   }
   else if( g == max )
   {
      hsv->h = 2.0 + ( b - r ) / delta;   // between cyan & yellow
   }
   else
   {
      hsv->h = 4.0 + ( r - g ) / delta;   // between magenta & cyan
   }

   hsv->h /= 6.0;            // [0-1]
   if( hsv->h < 0.0 )
   {
      hsv->h += 1.0;
   }
}
