
CXXFLAGS += -std=c++11 -g -Wall -pedantic $(shell fltk-config --use-images --cflags) $(shell pkg-config --cflags gdlib) -Isubs
CFLAGS += -g -Wall -pedantic
LDFLAGS += $(shell fltk-config --use-images --ldflags) $(shell pkg-config --libs gdlib)
PROJECT = mandelbrotviewer

all: $(PROJECT)

win:
	make -C mxe

release:
	make CXXFLAGS=-Os LDFLAGS=-s

OBJS = $(PROJECT).o
OBJS += \
	subs/cJSON.o \
	subs/gen_mandelbrot_png.o \
	subs/hsv_to_rgb.o \
	subs/make_gradient.o \
	subs/mandelbrot.o \
	subs/pal2name.o \
	subs/palettes.o \
	subs/pal_list.o \
	subs/rgb_to_hsv.o

$(PROJECT): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

$(PROJECT).o: $(PROJECT).cxx $(PROJECT).hxx
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.cxx
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(PROJECT).cxx: $(PROJECT).fl
	fluid -c $<

$(PROJECT).hxx: $(PROJECT).fl
	fluid -c $<

clean:
	make -C mxe clean
	rm -f $(PROJECT) */*.o *.o $(PROJECT).cxx $(PROJECT).hxx

