 Mandelbrot Viewer
===================

A very simple program to create an image of a
[Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set)
by setting up a few parameters and seleting one of the preinstalled
color palette.

It also allows exporting a PNG image of a different size than the preview.
Load and save of the current configuration is also implemented as a JSON
file for doing a more detailed export at a later time.

 Compiling
-----------

This piece of software requires two major libraries:
* [GD Graphics Library](https://libgd.github.io/)
* [Fast Light Toolkit](https://www.fltk.org/)

A Windows version can be compiled using [MXE](https://mxe.cc/).

